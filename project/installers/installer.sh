#!/bin/sh
echo "Beginning installation of Bombora...."
mkdir ~/.bombora
cp -r evaluator ~/.bombora/evaluator
cp -r translation ~/.bombora/translation
echo "alias bombora='~/.bombora/evaluator/evaluator'" >> ~/.bashrc
echo "alias bombora='~/.bombora/evaluator/evaluator'" >> ~/.zshrc
echo "Successfully installed Bombora..."
echo "Click Quit to exit"

