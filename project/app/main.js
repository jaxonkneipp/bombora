'use strict'

const electron = require('electron');

const app = electron.app;

const BrowserWindow = electron.BrowserWindow;
let mainWindow;

require('electron-reload')(__dirname);

function createWindow() {
  mainWindow = new BrowserWindow({ width: 1300, height: 780, minWidth: 1300, minHeight: 780});
  mainWindow.loadFile('index.html');
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    app.quit();
});

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow();
    }
});
