var app = require('electron').remote;
var dialog = app.dialog;
var fs = require('fs');

var current_filepath = null;

function updateFilePathLabel() {

    if (current_filepath) {
        document.getElementById("filename").innerText = current_filepath.split("/").slice(-1)[0];
    } else {
        document.getElementById("filename").innerText = "New file"
    }

}

function readFile(filepath) {

    fs.readFile(filepath, 'utf-8', (err, data) => {
        if (err) {
            alert("There was an error reading that file!")
        } else {
            document.getElementById("file_content").value = data;
            current_filepath = filepath;
            updateFilePathLabel();
        }
    });
}

function showNotification(title, message) {

    let myNotification = new Notification(title, {
        body: message
    })

}

function runCode() {

    if (current_filepath) {

        saveFile();

        var cmd=require('node-cmd');
 
        cmd.get(
            '~/.bombora/evaluator/evaluator -f ' + current_filepath,
            function(err, data, stderr){
                if (err) {
                    showNotification("Failed to compile!", "An error was encountered when trying to run your script")
                    console.log(err)
                    return  
                };
                showNotification("Successful compile!", "Your script successfully compiled")
                document.getElementById("output_content").innerText = data;
            }
        );

    } else {

        saveFile();

    }
    
}

function saveFile() {

    if (current_filepath) {
        var content = document.getElementById("file_content").value;
        fs.writeFile(current_filepath, content, (err) => {
            if (err) {
                alert(err);
                return;
            }
        })
    } else {

        dialog.showSaveDialog({

            filters: [{
              name: 'Bombora file',
              extensions: ['bom']
            }]

          }, (fileName) => {

            if (fileName == undefined) {
                return;
            }
    
            var content = document.getElementById("file_content").value;
    
            fs.writeFile(fileName, content, (err) => {
                if (err) {
                    alert(err);
                    return;
                }
                current_filepath = fileName;
                updateFilePathLabel();
            })    
        });

    }

    return true;

}

function createFile() {
    if (current_filepath) {
        saveFile();
    }
    current_filepath = null
    updateFilePathLabel();
    document.getElementById("file_content").value = "";
}

function recordAudio() {

    document.getElementById("record_container").classList.add("pulsate");

    var cmd=require('node-cmd');
 
    cmd.get(
        '~/.bombora/translation/translation -l',
        function(err, data, stderr){
            if (err) {
                showNotification("Audio reading unsuccessful!", "Your audio was unsuccessfully translated")
                document.getElementById("record_container").classList.remove("pulsate");
                console.log(err)
                return  
            };
            showNotification("Audio registered!", "Your audio was successfully translated")
            document.getElementById("file_content").value += '\n' + data;
            document.getElementById("record_container").classList.remove("pulsate");
        }
    );

}

document.getElementById('new_container').addEventListener('click', function(event){
    createFile();
});

document.getElementById('open_container').addEventListener('click', function(event){

    dialog.showOpenDialog((fileNames) => {
        if (fileNames == undefined) {
            return;
        } else {
            readFile(fileNames[0]);
        }
    });
    
});

document.getElementById('save_container').onclick = () => {
    saveFile();
};

document.getElementById('run_container').addEventListener('click', function(event){

    runCode();

});

document.getElementById('record_container').addEventListener('click', function(event){
    
    recordAudio();

});