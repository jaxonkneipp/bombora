'''
 _____ _____ _____ _____ _____ _____ _____ 
| __  |     |     | __  |     | __  |  _  |
| __ -|  |  | | | | __ -|  |  |    -|     |
|_____|_____|_|_|_|_____|_____|__|__|__|__|
    The spoken programming language
    Developed by Jaxon Kneipp 2018

evaluator.py

This module implements the objectifying functionality of the language. Its functionaility
takes tokens returned from the lexar and converts this into representative objects which can 
be used by the evaluator.

'''

#-----------------------------------------------------------------------------------
#	IMPORT STATEMENTS
#-----------------------------------------------------------------------------------
from lexar import *
from parser import *
from identifiers import *
import sys
import argparse

#-----------------------------------------------------------------------------------
#	GLOBAL VARIABLES
#-----------------------------------------------------------------------------------

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file", action='store', required=True, help="Location of code file")
args = parser.parse_args()

code = open(args.file, 'r').read()

tokens = tokenise(code)
objects = objectify(tokens)

'''
print("\n************************* TOKENS *************************\n")
print(tokens)

print("\n************************* OBJECTS *************************\n")
print([type(i) for i in objects.items])
'''

#-----------------------------------------------------------------------------------
#	FUNCTION DEFINITIONS
#-----------------------------------------------------------------------------------

def evaluate_expression(tokens, context) -> int:

    tokens_temp = tokens.copy()

    for index, token in enumerate(tokens_temp):
        if token[0] == STR:
            tokens_temp[index] = (INT, context[token[1]])

    oporators = {"divided by":"/", "times":"*", "plus":"+", "minus":"-"}

    for oporator in oporators.keys():
        try:
            index = tokens_temp.index((OP, oporator))
            while index:
                tokens_temp[index-1] = (INT, eval(str(tokens_temp[index-1][1])+oporators[oporator]+str(tokens_temp[index+1][1])))
                tokens_temp.pop(index)
                tokens_temp.pop(index)
                index = tokens_temp.index((OP, oporator))
        except:
            pass
    
    return int(tokens_temp[0][1])

def evaluate_data_type(obj, context):

    if isinstance(obj, string_node):
        return str(obj.value)
    
    if isinstance(obj, int_node):
        return int(obj.value)

    if isinstance(obj, expression_node):
        return evaluate_expression(obj.expression, context)

def evaluate(obj, context={}):

    output = ""

    if isinstance(obj, group_node):

        for currend_obj in obj.items:

            evaluated = evaluate(currend_obj, context)
            output += evaluated[0]

    if isinstance(obj, variable_node):

        context[obj.name] = evaluate_data_type(obj.value, context)

    if isinstance(obj, print_node):
        
        output += str(evaluate_data_type(obj.item, context)) + "\n"

    if isinstance(obj, if_node):

        operater = obj.condition.operator.operator
        if operater == EQUAL_TO:
            if evaluate_data_type(obj.condition.argument_1, context) == evaluate_data_type(obj.condition.argument_2, context):
                output += str(evaluate(obj.group, context)[0])
        elif operater == LESS_THEN:
            if evaluate_data_type(obj.condition.argument_1, context) < evaluate_data_type(obj.condition.argument_2, context):
                output += str(evaluate(obj.group, context)[0])
        elif operater == GREATER_THEN:
            if evaluate_data_type(obj.condition.argument_1, context) > evaluate_data_type(obj.condition.argument_2, context):
                output += str(evaluate(obj.group, context)[0])

    if isinstance(obj, for_node):

        iterable = list(eval("range(" + obj.iterable.lower +"," + obj.iterable.upper + ")"))
        for name in iterable:
            context_temp = context.copy()
            context_temp[obj.name] = name
            output += str(evaluate(obj.group, context_temp)[0])

        pass

    return (output, context)

output = evaluate(objects)

#print("\n************************* OUTPUT *************************\n")
print(output[0])
'''
print("\n************************* CONTEXT *************************\n")
print(output[1])
'''