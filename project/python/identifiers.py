#!/usr/bin/python3

'''
 _____ _____ _____ _____ _____ _____ _____ 
| __  |     |     | __  |     | __  |  _  |
| __ -|  |  | | | | __ -|  |  |    -|     |
|_____|_____|_|_|_|_____|_____|__|__|__|__|
    The spoken programming language
    Developed by Jaxon Kneipp 2018

identifiers.py

This module exists to provide system wide standards for block and type string identifiers

'''

# TYPES
ID = "id"
INT = "int"
EXP = "exp"
STR = "str"
OP = "op"

# IDENTIFIERS
SET = "set"
PRINT = "print"
IF = "if"
FOR = "for"
END = "end"
THEN = "then"
BEGIN = "begin"
START = "start"
RANGE = "range"

#OPERATORS
GREATER_THEN = "bigger then"
LESS_THEN = "less then"
EQUAL_TO = "equals"
IN = "in"
