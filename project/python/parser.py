#!/usr/bin/python3

'''
 _____ _____ _____ _____ _____ _____ _____ 
| __  |     |     | __  |     | __  |  _  |
| __ -|  |  | | | | __ -|  |  |    -|     |
|_____|_____|_|_|_|_____|_____|__|__|__|__|
    The spoken programming language
    Developed by Jaxon Kneipp 2018

parser.py

This module implements the objectifying functionality of the language. Its functionaility
takes tokens returned from the lexar and converts this into representative objects which can 
be used by the evaluator.

'''

#-----------------------------------------------------------------------------------
#	IMPORT STATEMENTS
#-----------------------------------------------------------------------------------

import lexar
from identifiers import *

#-----------------------------------------------------------------------------------
#	CLASS DEFINITIONS
#-----------------------------------------------------------------------------------

class Node:

    '''

        Defines a node

        __init__ Args:
            None

    '''

    pass

class group_node(Node):

    '''

        Defines a collection of other nodes

        __init__ Args:
            items: List of nodes included in the group node

    '''
    
    def __init__(self, items):

        self.items = items

class print_node(Node):

    '''

        Defines a vaue to be printed

        __init__ Args:
            item: The node representing a value to be printed

    '''

    def __init__(self, item):
        self.item = item

class if_node(Node):

    '''

        Defines a logic if statement

        __init__ Args:
            condition: The tokens representing the condition
            group: The group to be executed, should to condition be true

    '''

    def __init__(self, condition, group): 
        
        self.group = group
        self.condition = condition

class int_node(Node):

    '''

        Defines a integer

        __init__ Args:
            integer: The integer being stored

    '''

    def __init__(self, integer): 
        self.value = integer

class expression_node(Node):

    '''

        Defines a logic expression

        __init__ Args:
            expression: The tokens used in the expression

    '''

    def __init__(self, expression): 
        self.expression = expression

class operator_node(Node):

    '''

        Defines a logic expression

        __init__ Args:
            operator: The operator being stored

    '''

    def __init__(self, operator): 
        self.operator = operator

class string_node(Node):

    '''

        Defines a string

        __init__ Args:
            string: The string being stored

    '''

    def __init__(self, string):
        self.value = string

class variable_node(Node):

    '''

        Defines a variable

        __init__ Args:
            name: The name of the variable
            value: The node of the value being stored in the variable.

    '''
    
    def __init__(self, name, value):
        self.name = name
        self.value = value

class iterable_node(Node):

    def __init__(self, name, value):
        self.lower = name
        self.upper = value 


class for_node(Node):

    '''

        Defines a for loop

        __init__ Args:
            name: The name of the variable
            iterable: The node of the value being stored in the variable.
            group: The group object to be triggered in the loop

    '''
    
    def __init__(self, name, iterable, group):
        self.name = name
        self.iterable = iterable
        self.group = group

class condition_node(Node):

    '''

        Defines a variable

        __init__ Args:
            name: The name of the variable
            value: The node of the value being stored in the variable.

    '''
    
    def __init__(self, argument_1, operator, argument_2):
        self.argument_1 = argument_1
        self.operator = operator
        self.argument_2 = argument_2

#-----------------------------------------------------------------------------------
#	FUNCTION DEFINITIONS
#-----------------------------------------------------------------------------------

def objectify(tokens) -> group_node:

    '''

    Turns a list of tokens into nested python objects

    Args:
        tokens: The array of tokens 

    Returns:
        A group node containg children nodes for the program

    '''

    # Stores all the objects in the node
    items = []

    # current_index will store the current index in the list of tokens being converted
    current_index = 0

    # Loop while there are still more tokens to be converted
    while current_index < len(tokens): 

        # Reference the current token
        token = tokens[current_index]

        # Determine if the current token is the start of a new block
        if token[0] == ID:

            # Determine if the current token is setting a variable
            if token[1] == SET:

                # Reference the name of the variable
                name = tokens[current_index+1][1]

                # Move the current index to be the start of the value being assigned
                current_index += 3

                # Get the index of the token identifying the end of the set
                index_of_end = current_index + tokens[current_index:].index((END, SET))

                # Get all tokens being assigned as the value of the variable
                set_value = tokens[current_index:index_of_end]

                # Recursively get an object representing these values
                value = objectify(set_value).items[0]

                # Create node to represent the variable
                node = variable_node(name, value)

                # Add node to the items
                items.append(node)

                # Update index to be the end of the block
                current_index = index_of_end

            # Determine if the current token is a print
            if token[1] == PRINT:

                # Move the current index to be the start of the value being printed
                current_index += 1

                # Get the index of the token identifying the end of the print
                index_of_end = current_index + tokens[current_index:].index((END, PRINT))

                # Get all tokens being assigned as the value to be printed
                set_value = tokens[current_index:index_of_end]

                # Recursively get an object representing this value
                value = objectify(set_value).items[0]

                # Create node to represent the print
                node = print_node(value)

                # Add node to the items
                items.append(node)

                # Update index to be the end of the block
                current_index = index_of_end

            # Determine if the current token is an if block
            if token[1] == IF:

                # Get the index of the token representing the start of the block
                index_of_then = current_index + tokens[current_index:].index((OP, THEN))

                # Get all tokens being assigned as the condition to be met
                condition_tokens =  tokens[current_index+1:index_of_then]

                # create objects to represent condition
                condition_objects = objectify(condition_tokens)

                # Create condition node
                condition = condition_node(condition_objects.items[0], condition_objects.items[1], condition_objects.items[2])
                
                # Update current index to be the start of the block
                current_index = index_of_then

                # Get the index for the token representing the end of the if block
                index_of_end = current_index + tokens[current_index:].index((END, IF))

                # Get all tokens representing the block to be executed should the condition be met
                group_tokens = tokens[current_index:index_of_end]

                # Recursively get and object to represent these tokens
                group = objectify(group_tokens)

                # Create a node to represent the if
                node = if_node(condition, group)

                # Add the node to the items
                items.append(node)

                # Update index to be the end of the block
                current_index = index_of_end

            if token[1] == FOR:

                # Get the index of the token representing the start of the block
                index_of_begin = current_index + tokens[current_index:].index((OP, BEGIN))

                # Get token representing the name in the for loop
                name = tokens[current_index+1][1]
                
                # Create node to represent iterable
                iterable = iterable_node(tokens[current_index+3][1][0], tokens[current_index+3][1][1])

                # Get token representing the name in the for loop
                index_of_end = current_index + tokens[current_index:].index((END, FOR))

                # Update current index to be the start of the block
                current_index = index_of_begin

                # Recursively get and object to represent these tokens
                group_tokens = tokens[current_index+1:index_of_end]

                # Get token representing the name in the for loop
                group = objectify(group_tokens)

                # Get token representing the name in the for loop
                node = for_node(name, iterable, group)

                # Get token representing the name in the for loop
                items.append(node)

                # Get token representing the name in the for loop
                current_index = index_of_end
       
        # Determine if the current token is the start of a block data type
        elif token[0] == START:

            # Determine if the current token is the start of a string
            if token[1] == STR:
                
                # Create node to represent string content
                node = string_node(tokens[current_index+1][1])

                # Add the node to the items
                items.append(node)

                # Update index to be the end of the block
                current_index += 2

            # Determine if the current token is the start of an expression
            elif token[1] == EXP:
                
                # Move index to the start of the expression
                current_index += 1

                # Get the index of the token representing the end of the expression
                index_of_end = current_index + tokens[current_index:].index((END, EXP))

                # Get the tokens representing the contents of the expression
                expression = tokens[current_index:index_of_end]

                # Create node to represent the expression
                node = expression_node(expression)

                # Add the node to the items
                items.append(node)

                # Update index to be the end of the block
                current_index = index_of_end

        elif token[0] == OP:

            # Create node to represent the operator
            node = operator_node(token[1])

            # Add the node to the items
            items.append(node)

            # Update index to be the end of the block
            current_index += 1
        
        # Determine if the token is an integer   
        elif token[0] == INT:

            # Create node to represent the integer
            node = int_node(tokens[current_index][1])

            # Add the node to the items
            items.append(node)

            # Update index to be the end of the block
            current_index += 1

        else:

            # Update index to the next token
            current_index += 1

    # Return the group node
    return group_node(items)
