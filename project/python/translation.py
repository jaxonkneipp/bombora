#!/usr/local/bin/python3 

'''
 _____ _____ _____ _____ _____ _____ _____ 
| __  |     |     | __  |     | __  |  _  |
| __ -|  |  | | | | __ -|  |  |    -|     |
|_____|_____|_|_|_|_____|_____|__|__|__|__|
    The spoken programming language
    Developed by Jaxon Kneipp 2018

translation.py

This module implements the capturing and translation of the spoken language into
English text. 

'''
import speech_recognition
import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--listen", action='store_true', required=False, help="Location of the audio file")
parser.add_argument("-f", "--file", action='store', required=False, help="Location of the audio file")
args = parser.parse_args()

recogniser = speech_recognition.Recognizer()

if args.listen:

    microphone = speech_recognition.Microphone()

    with microphone as source:
        audio = recogniser.listen(source)

    try:  
        print(recogniser.recognize_google(audio))  
    except sr.UnknownValueError:  
        print("###### ERROR UNDERSTANDING AUDIO ######")  

elif args.file:

    file = speech_recognition.AudioFile(args.file)                 
    with file as source:
        recogniser.adjust_for_ambient_noise(source)
        audio = recogniser.record(source)

    text = recogniser.recognize_sphinx(audio)
    print(text)