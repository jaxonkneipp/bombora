#!/usr/bin/python3

import re

input_file_path = "input/1.txt"

MULTIPLY = "*"
DIVISION = "/"
ADD = "+"
SUBTRACT = "-"

OPORATION_IDENTIFIERS = [

                    (DIVISION, "/", "([^\+\*\-\/\s]+) divided by ([^\+\*\-\/\s]+)"),
                    (MULTIPLY, "*" , "([^\+\*\-\/\s]+) times ([^\+\*\-\/\s]+)"), 
                    (ADD, "+", "([^\+\*\-\/\s]+) plus ([^\+\*\-\/\s]+)"),
                    (SUBTRACT, "-", "([^\+\*\-\/\s]+) minus ([^\+\*\-\/\s]+)")
                    
                    ]


def equate(equation: str) -> int:

    RE_ALPHA = re.compile('(\d.)|(\btimes\b)|(\bplus\b)|(\bdivided by\b)|(\bminus\b)')
    contains_variables = RE_ALPHA.sub('', equation)
    print(contains_variables)

    for op in OPORATION_IDENTIFIERS:
        match = re.compile(op[2]).search(equation)
        while match:
            equation = equation.replace(match.group(0), str(eval(match.group(1)+op[1]+match.group(2))))
            match = re.compile(op[2]).search(equation)
    return int(equation)
    