#!/usr/bin/python3

'''
 _____ _____ _____ _____ _____ _____ _____ 
| __  |     |     | __  |     | __  |  _  |
| __ -|  |  | | | | __ -|  |  |    -|     |
|_____|_____|_|_|_|_____|_____|__|__|__|__|
    The spoken programming language
    Developed by Jaxon Kneipp 2018

lexar.py

This module implements the tokenisation functionality of the language. Its functionaility
takes strings of bombora code and converts this into representative tokens which can be used
by the parser and evaluator.

'''

#-----------------------------------------------------------------------------------
#	IMPORT STATEMENTS
#-----------------------------------------------------------------------------------

import re
from identifiers import *

#-----------------------------------------------------------------------------------
#	GLOBAL VARIABLES
#-----------------------------------------------------------------------------------

# Stores data for the major blocks in the Bombora language
BLOCK_IDENTIFIERS = [

                    (SET, "set ([^\s]+) to (.+?) end set"), 
                    (PRINT, "print (.+?) end print"),
                    (EXP, "start expression (.*) end expression"),
                    (IF, "if (.+)? (equals|bigger then|less then) (.+)? then"),
                    (FOR, "for (.+)? in (.+)? begin"),
                    (END, "end (if|for)")
                    
                    ]

#-----------------------------------------------------------------------------------
#	FUNCTION DEFINITIONS
#-----------------------------------------------------------------------------------

def tokenise_expression(expression: str) -> [(str, str)]:

    '''

    Takes a Bombora expression string and returns a token or tokens comprising the expression

    Args:
        expression: The string containing the bombora expression

    Returns:
        An array of tokens representative of the bombora expression

    '''

    # Stores tokens
    tokens = []

    # Stores all possible oporators
    operations = ['times', 'plus', 'minus', 'divided by']

    # Loop through all block in the passed string
    for block in expression.split(" "):

        # Determine if the block is an oportation
        if block in operations:
            # Add token identifying the oporator
            tokens.append((OP, block))
        # Determine if the block is an integer
        elif block.isdigit():
            # Add token identifying the integer
            tokens.append((INT, block))
        # Determine if the block is something else...most likely a variable name :)
        else:
            # Add token identifying the unknown thing...a string?
            tokens.append((STR, block))

    return tokens

def tokenise_values(value: str) -> [(str, str)]:

    '''

    Takes a Bombora data type string and returns a token or tokens conforming to the Bombora data type

    Args:
        value: The string containing the bombora data type

    Returns:
        An array of tokens representative of the passed parameters data type.

    '''

    # Stores data for the major types in the Bombora language
    TYPE_IDENTIFIERS = [

                    (INT, "^[0-9]*$"), 
                    (START, "start (string|expression) (.*) end (string|expression)$"),
                    (RANGE, "range (.+)? to (.+)? end range"),
                    (OP, "^(to|bigger then|less then|equals)$")
                    
                    ]

    # Search through all potential types
    for type in TYPE_IDENTIFIERS:

        # Stores tokens
        tokens = []

        # Try and match value to the current type
        match = re.compile(type[1], re.MULTILINE).match(value)

        # Determine if a match was successful
        if match:

            # Determine if the data type is a block (i.e. multiple tokens)
            if type[0] == START:

                # Determine if the data type matched was a string
                if match.group(1) == 'string':

                    # Add token to identify a string
                    tokens.append((START, STR))
                    # Add token to identify the string
                    tokens.append((STR, match.group(2)))
                    # Add token to identify the end of a string
                    tokens.append((END, STR))

                # Determine if the data type matched was an expression
                elif match.group(1) == 'expression':

                    # Add token to identify an expression
                    tokens.append((START, EXP))
                    # Add tokens for the oporators and values being assigned to the expression
                    for token in tokenise_expression(match.group(2)):
                        tokens.append(token)
                    # Add token to identify the end of an expression
                    tokens.append((END, EXP))

            elif type[0] == RANGE:

                tokens.append((RANGE, [match.group(1), match.group(2)])) 

            else:

                # Add token to identify the type matched
                tokens.append((type[0], value))

            return tokens

    # By default return the data type as a string
    return [(STR, value)]

def tokenise(code: str) -> list:

    '''

    Converts a valid Bombora string into tokens

    Args:
        code: This is the string on Bombora code.

    Returns:
        An array of tuples, representing tokens.

    '''

    # Strips string of unnecessary whitespace
    code = ' '.join(code.rstrip().lower().replace('\n', '').split())

    # tokens will store all current tokens
    tokens = []

    # current_index will store the current index in the string being converted
    current_index = 0

    def add_token(block: list) -> int:

        '''
        Adds a token to the tokens variable

        Args:
            block: A tuple containing the block identifier and the regex match for the block

        Returns:
            An index at the end of the match passed in the tuple

        '''

        # Reference the regex match for the block
        match = block[1]

        # Determine if the match is a SET
        if (block[0] == SET):

            # Add token to identify a set
            tokens.append((ID, SET))
            # Add token to identify the variables name
            tokens.append((STR, match.group(1)))
            # Add token to identify the assigning oportator
            tokens.append((OP, "to"))
            # Add tokens for the value being assigned to the variable
            for token in tokenise_values(match.group(2)):
                tokens.append(token)
            # Add token to identify the end of the set
            tokens.append((END, SET))

        # Determine if the match is a PRINT
        elif (block[0] == PRINT):

            # Add token to identify a print
            tokens.append((ID, PRINT))
            # Add tokens for the value being assigned to the print
            for token in tokenise_values(match.group(1)):
                tokens.append(token)
            # Add token to identify the end of the print
            tokens.append((END, PRINT))

        # Determine if the match is an IF
        elif (block[0] == IF):

            # Add token to identify an if
            tokens.append((ID, IF))
            # Add tokens for the value being assigned to the first part of the condition
            for token in tokenise_values(match.group(1)):
                tokens.append(token)
            # Add token being assigned as the operator in the condition
            for token in tokenise_values(match.group(2)):
                tokens.append(token)
            # Add tokens for the value being assigned to the second part of the condition
            for token in tokenise_values(match.group(3)):
                tokens.append(token)
            # Add token to identify the beinginng of the group in an if
            tokens.append((OP, THEN))

        # Determine if the match is an IF
        elif (block[0] == FOR):

            tokens.append((ID, FOR))

            for token in tokenise_values(match.group(1)):
                tokens.append(token)

            tokens.append((OP, IN))

            for token in tokenise_values(match.group(2)):
                tokens.append(token)

            tokens.append((OP, BEGIN))

        # Determine if the match is an END
        elif (block[0] == END):

            # Determine if the end is the end of an if
            if match.group(1) == "if":
                # Add token to identify the end of the if
                tokens.append((END, IF))

            elif match.group(1) == "for":
                # Add token to identify the end of the if
                tokens.append((END, FOR))
        
        # Return the end index of the match passed in the block
        return match.end()
        
    while current_index < len(code)-1:
        
        # Get code from current position onwards
        code_remaining = code[current_index:]

        # Blocks will store all major code blocks in the remaining string
        blocks = []

        # Search through all potential blocks
        for block in BLOCK_IDENTIFIERS:

            # Return the result for the block currently in focus
            blocks.append((block[0], re.compile(block[1], re.MULTILINE).search(code_remaining)))

        # Check if blocks is empty (No blocks left in string)  
        if not blocks:

            # Print error message
            print("Invalid Syntax: " + code_remaining)

            # Crash the program
            exit()

        # Remove results with returned None
        blocks = [block for block in blocks if block[1]]

        # Sort from closest to index
        blocks = sorted(blocks, key=lambda block: block[1].start())

        # Get next block
        next_block = blocks[0]

        # Add block to tokens and update the current index
        current_index += add_token(next_block)
        
    # Return tokens
    return tokens
    